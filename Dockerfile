FROM openjdk:8
COPY target/simple-1.0-SNAPSHOT-jar-with-dependencies.jar /usr/src/myapp/simple-1.0-SNAPSHOT-jar-with-dependencies.jar
WORKDIR /usr/src/myapp
CMD ["java","-jar","simple-1.0-SNAPSHOT-jar-with-dependencies.jar"]
