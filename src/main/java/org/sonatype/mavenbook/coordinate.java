package org.sonatype.mavenbook;

public class coordinate {
	
	private int x;
	private int y;
	
	
	public coordinate(int x_in, int y_in){
		this.x = x_in;
		this.y = y_in;
	}
	
	
	public int get_x(){
		return x;
	}
	
	public int get_y(){
		return y;
	}
	
	public void set_x(int x_in){
		this.x = x_in;
	}
	
	public void set_y(int y_in){
		this.y = y_in;
	}
	
	public void set_coordinates(int x_in, int y_in){
		this.x = x_in;
		this.y = y_in;	
	}
	
	public void increment_coordinates(int a, int b){
		int k = get_x();
		int l = get_y();
		set_coordinates(k+a,l+b);
	}
	
	public void print_coords(){
		System.out.println("("+x+","+y+")");
		
	}
	
}