package org.sonatype.mavenbook;

import static java.lang.System.out;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FilePrint {
	
	public static void ConsoleDump(String File_In) throws IOException {
	
	InputStream inputStream = FilePrint.class.getResourceAsStream(File_In);
		InputStreamReader inputReader = new InputStreamReader(inputStream);
		BufferedReader reader = new BufferedReader(inputReader);
		String line = null;
		while ((line = reader.readLine()) != null){
			out.println(line);
		}
	}
}