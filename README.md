# BOUNCY BALL

## THE ULTIMATE BOUNCE

## NEVER BEFORE HAS SUCH AN EXPERIENCE GRACED YOUR SHELL

Assuming you have maven installed type the following:

```
mvn install
```

Assuming you have docker engine installed and running, you need to build and run the docker image:
```
docker build -t [yourImageNameofChoice]
docker run -it --rm [yourImageNameofChoice]
```

## KEEP ON BOUNCING!!!!
